
/* 
a. find all artists with letter d on it's name
 */

DESCRIBE artists;

-- ANSWER:
SELECT * FROM artists WHERE name LIKE "%d%";
-- RESULT:
/* 
MariaDB [music_db]> SELECT * FROM artists WHERE name LIKE "%d%";
+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady Gaga     |
|  6 | Ariana Grande |
|  8 | Ed Sheeran    |
+----+---------------+
3 rows in set (0.001 sec)
 */


/* 
b. Find all songs that has a length of less than 4 minutes.
 */
-- ANSWER:
 SELECT * FROM songs WHERE song_length < 400;
 -- RESULT:
 /* 
 MariaDB [music_db]> SELECT * FROM songs WHERE song_length < 400;
+----+----------------+-------------+--------------------------------------+----------+
| id | song_name      | song_length | genre                                | album_id |
+----+----------------+-------------+--------------------------------------+----------+
|  1 | Gangname Style | 00:03:39    | K-POP                                |        1 |
|  4 | Love Story     | 00:03:55    | Country Pop                          |        3 |
|  6 | Red            | 00:03:41    | Country                              |        4 |
|  7 | Black Eyesy    | 00:03:04    | Rock and Roll                        |        5 |
|  8 | Shallow        | 00:03:36    | Country, Rock, Folk Rock             |        5 |
| 10 | Sorry          | 00:03:20    | Dancehall-poptropical Housemoombahto |        7 |
| 12 | Thank U, Next  | 00:03:27    | Pop, R&B                             |        8 |
| 13 | Boyfriend      | 00:02:52    | pop                                  |        8 |
| 14 | 24k Magic      | 00:03:46    | Funk, Disco, R&B                     |        9 |
| 15 | Lost           | 00:03:21    | Pop                                  |       10 |
+----+----------------+-------------+--------------------------------------+----------+
10 rows in set (0.002 sec)
  */


/* 
c. 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
 */
 
 
 DESCRIBE albums;
 /* 
 MariaDB [music_db]> DESCRIBE albums;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| id            | int(11)     | NO   | PRI | NULL    | auto_increment |
| album_title   | varchar(50) | NO   |     | NULL    |                |
| date_released | date        | NO   |     | NULL    |                |
| artist_id     | int(11)     | NO   | MUL | NULL    |                |
+---------------+-----------
  */
 DESCRIBE songs;
 /* 
 MariaDB [music_db]> DESCRIBE songs;
+-------------+-------------+------+-----+---------+----------------+
| Field       | Type        | Null | Key | Default | Extra          |
+-------------+-------------+------+-----+---------+----------------+
| id          | int(11)     | NO   | PRI | NULL    | auto_increment |
| song_name   | varchar(50) | NO   |     | NULL    |                |
| song_length | time        | NO   |     | NULL    |                |
| genre       | varchar(50) | NO   |     | NULL    |                |
| album_id    | int(11)     | NO   | MUL | NULL    |                |
+-------------+-------------+------+-----+---------+----------------+
5 rows in set (0.013 sec)
  */

-- ANSWER:
SELECT albums.album_title, songs.song_name, songs.song_length FROM albums
    LEFT JOIN songs ON albums.id = songs.album_id
    UNION
SELECT albums.album_title, songs.song_name, songs.song_length FROM albums
    LEFT JOIN songs ON albums.id = songs.album_id


 -- RESULT:

/* 
MariaDB [music_db]> SELECT albums.album_title, songs.song_name, songs.song_length FROM albums
    ->     LEFT JOIN songs ON albums.id = songs.album_id
    ->     UNION
    -> SELECT albums.album_title, songs.song_name, songs.song_length FROM artists
    ->     RIGHT JOIN albums ON artists.id = albums.artist_id;
ERROR 1054 (42S22): Unknown column 'songs.song_name' in 'field list'
MariaDB [music_db]> SELECT albums.album_title, songs.song_name, songs.song_length FROM albums
    ->     LEFT JOIN songs ON albums.id = songs.album_id
    ->     UNION
    -> SELECT albums.album_title, songs.song_name, songs.song_length FROM albums
    ->     LEFT JOIN songs ON albums.id = songs.album_id
    -> ;
+-----------------+----------------+-------------+
| album_title     | song_name      | song_length |
+-----------------+----------------+-------------+
| Psy 6           | Gangname Style | 00:03:39    |
| Trip            | Kundiman       | 00:04:24    |
| Red             | Fearless       | 00:04:02    |
| Red             | Love Story     | 00:03:55    |
| Fearless        | State of Grace | 00:04:55    |
| Fearless        | Red            | 00:03:41    |
| A Star Is Born  | Black Eyesy    | 00:03:04    |
| A Star Is Born  | Shallow        | 00:03:36    |
| Born This Way   | Born This Way  | 00:04:20    |
| Purpose         | Sorry          | 00:03:20    |
| Purpose         | Into You       | 00:04:05    |
| Believe         | Thank U, Next  | 00:03:27    |
| Believe         | Boyfriend      | 00:02:52    |
| Dangerous Woman | 24k Magic      | 00:03:46    |
| Thank U, Next   | Lost           | 00:03:21    |
| 24k Magic       | NULL           | NULL        |
| Earth to Mars   | NULL           | NULL        |
+-----------------+----------------+-------------+
17 rows in set (0.003 sec)
 */






 /* 
d Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

  */

  DESCRIBE artists;
/* 
MariaDB [music_db]> DESCRIBE artists;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| name  | varchar(50) | NO   |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
2 rows in set (0.012 sec)
 */

  DESCRIBE albums;
  /* 
  MariaDB [music_db]> DESCRIBE albums;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| id            | int(11)     | NO   | PRI | NULL    | auto_increment |
| album_title   | varchar(50) | NO   |     | NULL    |                |
| date_released | date        | NO   |     | NULL    |                |
| artist_id     | int(11)     | NO   | MUL | NULL    |                |
+---------------+-------------+------+-----+---------+----------------+
4 rows in set (0.011 sec)
   */

  -- ANSWER:

SELECT artists.id, artists.name, albums.id, albums.album_title, albums.date_released, albums.artist_id FROM artists
    LEFT JOIN albums ON artists.id = albums.artist_id
    WHERE album_title LIKE "%a%";


-- RESULT:
/* 
MariaDB [music_db]> SELECT artists.id, artists.name, albums.id, albums.album_title, albums.date_released, albums.artist_id FROM artists
    ->     LEFT JOIN albums ON artists.id = albums.artist_id
    ->     WHERE album_title LIKE "%a%";
+----+---------------+----+-----------------+---------------+-----------+
| id | name          | id | album_title     | date_released | artist_id |
+----+---------------+----+-----------------+---------------+-----------+
|  3 | Taylor Swift  |  4 | Fearless        | 2008-11-11    |         3 |
|  4 | Lady Gaga     |  5 | A Star Is Born  | 2018-10-05    |         4 |
|  4 | Lady Gaga     |  6 | Born This Way   | 2011-05-23    |         4 |
|  6 | Ariana Grande |  9 | Dangerous Woman | 2016-05-20    |         6 |
|  6 | Ariana Grande | 10 | Thank U, Next   | 2019-02-08    |         6 |
|  7 | Bruno Mars    | 11 | 24k Magic       | 2016-11-18    |         7 |
|  7 | Bruno Mars    | 12 | Earth to Mars   | 2011-02-07    |         7 |
+----+---------------+----+-----------------+---------------+-----------+
7 rows in set (0.002 sec)
 */
 




/* 

e. Sort the albums in Z-A order. (Show only the first 4 records.)

 */

 DESCRIBE albums;
/* 
MariaDB [music_db]> DESCRIBE albums;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| id            | int(11)     | NO   | PRI | NULL    | auto_increment |
| album_title   | varchar(50) | NO   |     | NULL    |                |
| date_released | date        | NO   |     | NULL    |                |
| artist_id     | int(11)     | NO   | MUL | NULL    |                |
+---------------+-------------+------+-----+---------+----------------+
4 rows in set (0.013 sec)
 */


 -- ANSWER:
 SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- RESULT:
 /* 
 MariaDB [music_db]> SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
+----+---------------+---------------+-----------+
| id | album_title   | date_released | artist_id |
+----+---------------+---------------+-----------+
|  2 | Trip          | 1996-02-14    |         2 |
| 10 | Thank U, Next | 2019-02-08    |         6 |
|  3 | Red           | 2012-10-22    |         3 |
|  7 | Purpose       | 2015-11-13    |         5 |
+----+---------------+---------------+-----------+
4 rows in set (0.002 sec)
  */




  /* 
  f. Join the 'albums' and 'songs' tables and find all songs greater than 3 minutes 30 seconds. (Sort albums from Z-A)
   */


   -- ANSWER:
SELECT albums.id, albums.album_title, albums.date_released, albums.artist_id, songs.id, songs.song_name, songs.song_length, songs.genre, songs.album_id FROM albums
    LEFT JOIN songs ON albums.id = songs.album_id
    WHERE song_length >= 330
    ORDER BY album_title DESC;

   -- RESULT:

   /* 
   MariaDB [music_db]> SELECT albums.id, albums.album_title, albums.date_released, albums.artist_id, songs.id, songs.song_name, songs.song_length, songs.genre, songs.album_id FROM albums
    ->     LEFT JOIN songs ON albums.id = songs.album_id
    ->     WHERE song_length >= 330
    ->     ORDER BY album_title DESC;
+----+-----------------+---------------+-----------+------+----------------+-------------+------------------------------------+----------+
| id | album_title     | date_released | artist_id | id   | song_name      | song_length | genre                              | album_id |
+----+-----------------+---------------+-----------+------+----------------+-------------+------------------------------------+----------+
|  2 | Trip            | 1996-02-14    |         2 |    2 | Kundiman       | 00:04:24    | OPM                                |        2 |
|  3 | Red             | 2012-10-22    |         3 |    3 | Fearless       | 00:04:02    | Pop Rock                           |        3 |
|  3 | Red             | 2012-10-22    |         3 |    4 | Love Story     | 00:03:55    | Country Pop                        |        3 |
|  7 | Purpose         | 2015-11-13    |         5 |   11 | Into You       | 00:04:05    | EDM House                          |        7 |
|  1 | Psy 6           | 2012-07-15    |         1 |    1 | Gangname Style | 00:03:39    | K-POP                              |        1 |
|  4 | Fearless        | 2008-11-11    |         3 |    5 | State of Grace | 00:04:55    | Rock, Alternative Rock, Arena Rock |        4 |
|  4 | Fearless        | 2008-11-11    |         3 |    6 | Red            | 00:03:41    | Country                            |        4 |
|  9 | Dangerous Woman | 2016-05-20    |         6 |   14 | 24k Magic      | 00:03:46    | Funk, Disco, R&B                   |        9 |
|  6 | Born This Way   | 2011-05-23    |         4 |    9 | Born This Way  | 00:04:20    | Electropop                         |        6 |
|  5 | A Star Is Born  | 2018-10-05    |         4 |    8 | Shallow        | 00:03:36    | Country, Rock, Folk Rock           |        5 |
+----+-----------------+---------------+-----------+------+----------------+-------------+------------------------------------+----------+
10 rows in set (0.001 sec)
    */